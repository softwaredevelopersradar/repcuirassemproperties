﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Test
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Prop_OnPasswordChecked(object sender, bool e)
        {
            if (!e)       
                MessageBox.Show("Invalid password!");            
        }

        private void prop_OnLanguageChanged(object sender, DllCuirassemProperties.Models.Languages e)
        {
            MessageBox.Show("Language" +  e.ToString());
        }
    }
}
