﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace DllCuirassemProperties.Editors
{
    public class CustomEditor : PropertyEditor
    {
        Dictionary<Type, string> dictKeyDataTemplate = new Dictionary<Type, string>()
        {
            { typeof(Models.Local.EndPointConnection), "EndPointEditorKey"},
            { typeof(Models.Local.Common), "CommonLocalEditorKey"},
            { typeof(Models.Local.Map), "MapEditorKey" },
            { typeof(Models.Local.Diagram), "DiagramEditorKey" },
            { typeof(Models.Global.Common), "CommonGlobalEditorKey"},
            { typeof(Models.Global.RadioIntelegence), "RadioIntelegenceEditorKey" },
            { typeof(Models.Global.KalmanFilterForCoordinate), "KalmanFilterForCoordinateEditorKey"},
            { typeof(Models.Global.KalmanFilterForDelays), "KalmanFilterForDelaysEditorKey"},
            { typeof(Models.Global.Correlation), "CorrelationEditorKey"},
            { typeof(Models.Global.DRMFilters), "DRMFiltersEditorKey" },

        };

        public CustomEditor(string PropertyName, Type DeclaringType, Type typeProperty)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/DllCuirassemProperties;component/Editors/Templates.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource[dictKeyDataTemplate[typeProperty]];
        }
    }
}
