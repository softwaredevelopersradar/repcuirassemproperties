﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Xml;
using DllCuirassemProperties.Models;
using WpfPasswordControlLibrary;

namespace DllCuirassemProperties
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class ControlProperties : UserControl, INotifyPropertyChanged
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Events
        public event EventHandler<LocalProperties> OnLocalPropertiesChanged = (sender, newModel) => { };
        public event EventHandler<GlobalProperties> OnGlobalPropertiesChanged = (sender, newModel) => { };
        public event EventHandler<Languages> OnLanguageChanged = (object sender, Languages language) => { };
        public event EventHandler<FileTypes> OnFileTypeChanged = (object sender, FileTypes fileType) => { };
        public event EventHandler<bool> OnPasswordChecked = (sender, isCorrect) => { };
        #endregion

        #region Properties

        private bool isLocalSelected;
        public bool IsLocalSelected
        {
            get => isLocalSelected;
            set
            {
                if (isLocalSelected == value) return;
                isLocalSelected = value;
                OnPropertyChanged();
            }
        }

        private bool isValid;
        public bool IsValid
        {
            get => isValid;
            private set
            {
                if (isValid == value) return;
                isValid = value;
                OnPropertyChanged();
            }
        }

        private string errorMessage;
        public string ErrorMessage
        {
            get => errorMessage;
            set
            {
                if (errorMessage == value) return;
                errorMessage = value;
                OnPropertyChanged();
            }
        }

        private bool _showToolTip;
        public bool ShowToolTip
        {
            get => _showToolTip;
            set
            {
                if (_showToolTip == value) return;
                _showToolTip = value;
                OnPropertyChanged();
            }
        }

        private LocalProperties _savedLocal;
        private GlobalProperties _savedGlobal;

        public LocalProperties Local
        {
            get
            {
                return (LocalProperties)Resources["localProperties"];
            }
            set
            {
                if ((LocalProperties)Resources["localProperties"] == value)
                {
                    _savedLocal = value.Clone();
                    return;
                }
                ((LocalProperties)Resources["localProperties"]).Update(value);
                _savedLocal = Local.Clone();
            }
        }

        public GlobalProperties Global
        {
            get
            {
                return (GlobalProperties)Resources["globalProperties"];
            }
            set
            {
                if ((GlobalProperties)Resources["globalProperties"] == value)
                {
                    _savedGlobal = value.Clone();
                    return;
                }
                ((GlobalProperties)Resources["globalProperties"]).Update(value);
                _savedGlobal = Global.Clone();
            }
        }

        #endregion

        #region Language
        Dictionary<string, string> TranslateDic;

        private void SetDynamicResources(Languages newLanguage)
        {
            try
            {
                ResourceDictionary dict = new ResourceDictionary();
                switch (newLanguage)
                {
                    case Languages.EN:
                        dict.Source = new Uri("/DllCuirassemProperties;component/DynamicResources/StringResource.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case Languages.RU:
                        dict.Source = new Uri("/DllCuirassemProperties;component/DynamicResources/StringResource.RU.xaml",
                                           UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/DllCuirassemProperties;component/DynamicResources/StringResource.EN.xaml",
                                          UriKind.Relative);
                        break;
                }
                Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            {
                //TODO
            }

        }


        private void ChangeLanguage(Languages newLanguage)
        {
            SetDynamicResources(newLanguage);
            LoadDictionary();
            SetCategoryGlobalNames();
            SetCategoryLocalNames();
        }

        void LoadDictionary()
        {
            var translation = Properties.Resources.Translation;
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(translation);
            TranslateDic = new Dictionary<string, string>();
            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode x2Node in xRoot.ChildNodes)
            {
                if (x2Node.NodeType == XmlNodeType.Comment)
                    continue;

                // получаем атрибут ID
                if (x2Node.Attributes.Count > 0)
                {
                    XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                    if (attr != null)
                    {
                        foreach (XmlNode childnode in x2Node.ChildNodes)
                        {
                            // если узел - language
                            if (childnode.Name == Local.Common.Language.ToString())
                            {
                                if (!TranslateDic.ContainsKey(attr.Value))
                                    TranslateDic.Add(attr.Value, childnode.InnerText);
                            }

                        }
                    }
                }
            }
        }

        private void SetCategoryGlobalNames()
        {
            try
            {
                foreach (var nameCategory in PropertyGlobal.Categories.Select(category => category.Name).ToList())
                {
                    var prop = PropertyGlobal.Categories.FirstOrDefault(t => t.Name == nameCategory);
                    if (prop != null && TranslateDic.ContainsKey(nameCategory))
                        prop.HeaderCategoryName = TranslateDic[nameCategory];
                }
            }
            catch (Exception)
            {

            }
        }

        private void SetCategoryLocalNames()
        {
            try
            {
                foreach (var nameCategory in PropertyLocal.Categories.Select(category => category.Name).ToList())
                {
                    var prop = PropertyLocal.Categories.FirstOrDefault(t => t.Name == nameCategory);
                    if (prop != null && TranslateDic.ContainsKey(nameCategory))
                        prop.HeaderCategoryName = TranslateDic[nameCategory];
                }
            }
            catch (Exception)
            {

            }
        }
        #endregion

        #region Validation

        private void Validate()
        {
            bool isvalid = true;
            string error = "";
            Action<object> funcValidation = (object obj) =>
            {
                foreach (var property in obj.GetType().GetProperties())
                {
                    foreach (var subProperty in property.PropertyType.GetProperties())
                    {
                        try
                        {
                            if (!(property.Name == "DependencyObjectType" || property.Name == "Dispatcher" ))
                            {
                                if (!string.IsNullOrEmpty(((IDataErrorInfo)property.GetValue(obj))[subProperty.Name]))
                                {
                                    error += $"{TranslateDic[property.Name]}: {TranslateDic[subProperty.Name]}" + "\n";
                                    
                                    isvalid = false;
                                }
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
                IsValid = isvalid;
                ShowToolTip = !isvalid;
                ErrorMessage = error;
                return;
            };

            if (IsLocalSelected)
            {
                funcValidation(Local);
            }
            else
            {
                funcValidation(Global);
            }
        }

        #endregion

        #region Access

        private readonly WindowPass _accessWindowPass = new WindowPass();

        private bool _isAccessChecked = false;

        private void InitAccessWindow()
        {
            _accessWindowPass.OnEnterInvalidPassword += _accessWindowPass_OnEnterInvalidPassword;
            _accessWindowPass.OnEnterValidPassword += _accessWindowPass_OnEnterValidPassword;
            _accessWindowPass.OnClosePasswordBox += _accessWindowPass_OnClosePasswordBox;
            _accessWindowPass.Resources = Resources;
            _accessWindowPass.SetResourceReference(WindowPass.LablePasswordProperty, "labelPassword");
        }

        private void _accessWindowPass_OnEnterValidPassword(object sender, EventArgs e)
        {
            _isAccessChecked = true;
            Local.Common.Access = AccessTypes.Admin;
            _accessWindowPass.Hide();
            ChangeVisibilityLocalProperties(true);
            ChangeVisibilityGlobalProperties(true);
            OnPasswordChecked(this, true);
        }

        private void _accessWindowPass_OnClosePasswordBox(object sender, EventArgs e)
        {
            Local.Common.Access = AccessTypes.Operator;
        }

        private void _accessWindowPass_OnEnterInvalidPassword(object sender, EventArgs e)
        {
            Local.Common.Access = AccessTypes.Operator;
            _accessWindowPass.Hide();
            OnPasswordChecked(this, false);
        }

        private void CheckLocalAccess()
        {
            if (!_isAccessChecked)
            {
                Task.Run(() =>
                   Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                   {
                       Local.Common.Access = AccessTypes.Operator;
                       _accessWindowPass.ShowDialog();
                   }));
            }
            else
                _isAccessChecked = false;
        }

        public void SetPassword(string password)
        {
            _accessWindowPass.ValidPassword = password;
        }

        public void ChangeVisibilityLocalProperties(bool browsable)
        {
            PropertyLocal.Properties[nameof(LocalProperties.DB)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(LocalProperties.DF)].IsBrowsable = browsable;
        }

        public void ChangeVisibilityGlobalProperties(bool browsable)
        {
            PropertyGlobal.Properties[nameof(GlobalProperties.KalmanFilterForCoordinate)].IsBrowsable = browsable;
            PropertyGlobal.Properties[nameof(GlobalProperties.KalmanFilterForDelay)].IsBrowsable = browsable;
            PropertyGlobal.Properties[nameof(GlobalProperties.Correlation)].IsBrowsable = browsable;
            PropertyGlobal.Properties[nameof(GlobalProperties.DRMFilters)].IsBrowsable = browsable;
            PropertyGlobal.Properties[nameof(GlobalProperties.RadioIntelegence)].IsBrowsable = browsable;
        }

        private void _this_Loaded(object sender, RoutedEventArgs e)
        {
            ChangeVisibilityLocalProperties(false);
            ChangeVisibilityGlobalProperties(false);
        }

        #endregion

        #region Local Global methods

        private void InitLocalProperties()
        {
            _savedLocal = Local.Clone();
            _savedLocal.Common.PropertyChanged += SavedCommonLocal_PropertyChanged;

            Local.Common.PropertyChanged += CheckLanguage_PropertyChanged;
            Local.OnPropertyChanged += HandlerPropertyChanged;

            PropertyLocal.Editors.Add(new Editors.CustomEditor(nameof(LocalProperties.Common), typeof(LocalProperties), typeof(Models.Local.Common)));
            PropertyLocal.Editors.Add(new Editors.CustomEditor(nameof(LocalProperties.DB), typeof(LocalProperties), typeof(Models.Local.EndPointConnection)));
            PropertyLocal.Editors.Add(new Editors.CustomEditor(nameof(LocalProperties.DF), typeof(LocalProperties), typeof(Models.Local.EndPointConnection)));
            PropertyLocal.Editors.Add(new Editors.CustomEditor(nameof(LocalProperties.Map), typeof(LocalProperties), typeof(Models.Local.Map)));
            PropertyLocal.Editors.Add(new Editors.CustomEditor(nameof(LocalProperties.Diagram), typeof(LocalProperties), typeof(Models.Local.Diagram)));
        }

        private void InitGlobalProperties()
        {
            SetBindingAccess();
            _savedGlobal = Global.Clone();
            Global.EventPropertyChanged += HandlerPropertyChanged;

            PropertyGlobal.Editors.Add(new Editors.CustomEditor(nameof(GlobalProperties.Common), typeof(GlobalProperties), typeof(Models.Global.Common)));
            PropertyGlobal.Editors.Add(new Editors.CustomEditor(nameof(GlobalProperties.RadioIntelegence), typeof(GlobalProperties), typeof(Models.Global.RadioIntelegence)));
            PropertyGlobal.Editors.Add(new Editors.CustomEditor(nameof(GlobalProperties.KalmanFilterForCoordinate), typeof(GlobalProperties), typeof(Models.Global.KalmanFilterForCoordinate)));
            PropertyGlobal.Editors.Add(new Editors.CustomEditor(nameof(GlobalProperties.KalmanFilterForDelay), typeof(GlobalProperties), typeof(Models.Global.KalmanFilterForDelays)));
            PropertyGlobal.Editors.Add(new Editors.CustomEditor(nameof(GlobalProperties.Correlation), typeof(GlobalProperties), typeof(Models.Global.Correlation)));
            PropertyGlobal.Editors.Add(new Editors.CustomEditor(nameof(GlobalProperties.DRMFilters), typeof(GlobalProperties), typeof(Models.Global.DRMFilters)));
        }

        private void SetBindingAccess()
        {
            Binding binding = new Binding();
            binding.Source = Local.Common;
            binding.Path = new PropertyPath(nameof(Local.Common.Access));
            binding.Mode = BindingMode.TwoWay;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            BindingOperations.SetBinding(Global, GlobalProperties.AcssesProperty, binding);
        }

        private void HandlerPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Validate();
        }
        private void CheckLanguage_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(Models.Local.Common.Language):
                    ChangeLanguage(Local.Common.Language);
                    OnLanguageChanged(sender, Local.Common.Language);
                    break;
                case nameof(Models.Local.Common.Access):


                    if (Local.Common.Access == AccessTypes.Admin)
                    {
                        CheckLocalAccess();
                    }
                    else
                    {
                        ChangeVisibilityLocalProperties(false);
                        ChangeVisibilityGlobalProperties(false);
                    }
                    break;
                default:
                    break;
            }
        }

        private void SavedCommonLocal_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(Models.Local.Common.Language):
                    OnLanguageChanged(sender, _savedLocal.Common.Language);
                    ChangeLanguage(_savedLocal.Common.Language);
                    break;
                case nameof(Models.Local.Common.FileType):
                    OnFileTypeChanged(sender, _savedLocal.Common.FileType);
                    break;
                default:
                    break;
            }

        }
        #endregion

        public ControlProperties()
        {
            InitializeComponent();
            InitLocalProperties();
            InitGlobalProperties();
            InitAccessWindow();
            ChangeLanguage(_savedLocal.Common.Language);
            //ChangeVisibilityLocalProperties(false);
            SetPassword("1111");
            PropertyLocal.Properties[nameof(LocalProperties.Diagram)].IsBrowsable = false; //Скрывает категорию Диаграма с полем График
        }

        private void ApplyClick(object sender, RoutedEventArgs e)
        {
            if (IsLocalSelected)
            {
                if (!_savedLocal.EqualTo(Local))
                {
                    _savedLocal.Update(Local.Clone());
                    OnLocalPropertiesChanged(this, _savedLocal.Clone());
                }
            }
            else
            {
                if (!_savedGlobal.EqualTo(Global))
                {
                    _savedGlobal.Update(Global.Clone());
                    OnGlobalPropertiesChanged(this, _savedGlobal.Clone());
                }
            }
        }

        private void NotApplyClick(object sender, RoutedEventArgs e)
        {
            if (IsLocalSelected)
            {
                if (!_savedLocal.EqualTo(Local))
                {
                    Local.Update(_savedLocal.Clone());
                    ChangeVisibilityLocalProperties(Local.Common.Access == AccessTypes.Admin);
                    ChangeVisibilityGlobalProperties(Local.Common.Access == AccessTypes.Admin);
                }
            }
            else
            {
                if (!_savedGlobal.EqualTo(Global))
                {
                    Global.Update(_savedGlobal.Clone());
                }
            }
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Validate();
        }    
    }
}
