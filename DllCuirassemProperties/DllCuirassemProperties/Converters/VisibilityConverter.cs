﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace DllCuirassemProperties.Converters
{
    public class VisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool flag = (bool)value;
            if (flag)
                return Visibility.Visible;

            return Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return true;

            if ((Visibility)value == Visibility.Visible)
                return true;
            return false;
        }
    }
}
