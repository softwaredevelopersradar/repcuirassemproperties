﻿using System.ComponentModel;

namespace DllCuirassemProperties.Models
{
    public enum FileTypes : byte
    {
        TXT,
        EXCEL,
        DOC,
        PDF
    }

    public enum Languages : byte
    {
        [Description("Русский")]
        RU,
        [Description("English")]
        EN
    }

    public enum AccessTypes : byte
    {
        Admin,
        Commandir,
        Operator
    }

    public enum Projections : byte
    {
        Geo = 1,
        Mercator = 2
    }

    public enum SamplingFrequency : int
    {
        [Description("31.25e6")]
        f31_25e6 = 31250000,
        [Description("62.5e6")]
        f62_5e6 = 62500000,
        [Description("125e6")]
        f125e6 = 125000000
    }
}
