﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DllCuirassemProperties.Models.Global
{
    public class DRMFilters : AbstractBaseProperties<DRMFilters>
    {
        private float alpha = 0.95f;
        private bool isImSolution = true;
        private short startElevation = 50;


        public bool IsImSolution
        {
            get => isImSolution;
            set
            {
                if (isImSolution == value) return;
                isImSolution = value;
                OnPropertyChanged();
            }
        }

        [Range(10, 800)]
        public short StartElevation
        {
            get => startElevation;
            set
            {
                if (startElevation == value) return;
                startElevation = value;
                OnPropertyChanged();
            }
        }

        [Range(0.1, 1)]
        public float Alpha
        {
            get => alpha;
            set
            {
                if (alpha == value) return;
                alpha = value;
                OnPropertyChanged();
            }
        }

        public override DRMFilters Clone()
        {
            return new DRMFilters
            {
                IsImSolution = IsImSolution,
                StartElevation = StartElevation,
                Alpha = Alpha
            };
        }

        public override bool EqualTo(DRMFilters model)
        {
            return model.StartElevation == StartElevation
                && model.Alpha == Alpha
                && model.IsImSolution == IsImSolution;
        }

        public override void Update(DRMFilters model)
        {
            StartElevation = model.StartElevation;
            Alpha = model.Alpha;
            IsImSolution = model.IsImSolution;
        }
    }
}
