﻿using System.ComponentModel.DataAnnotations;

namespace DllCuirassemProperties.Models.Global
{
    public class Common : AbstractBaseProperties<Common>
    {
        #region IModelMethods

        public override Common Clone()
        {
            return new Common
            {
                CenterZoneLatitude = CenterZoneLatitude,
                CenterZoneLongitude = CenterZoneLongitude,
                ZoneAlarm = ZoneAlarm,
                ZoneAttention = ZoneAttention,
                OperatorLocationLatitude = OperatorLocationLatitude,
                OperatorLocationLongitude = OperatorLocationLongitude
            };
        }

        public override bool EqualTo(Common model)
        {
            return model.CenterZoneLatitude == CenterZoneLatitude
                && model.CenterZoneLongitude == CenterZoneLongitude
                && model.ZoneAlarm == ZoneAlarm
                && model.ZoneAttention == ZoneAttention
                && model.OperatorLocationLongitude == OperatorLocationLongitude
                && model.OperatorLocationLatitude == OperatorLocationLatitude;
        }

        public override void Update(Common model)
        {
            CenterZoneLatitude = model.CenterZoneLatitude;
            CenterZoneLongitude = model.CenterZoneLongitude;
            ZoneAlarm = model.ZoneAlarm;
            ZoneAttention = model.ZoneAttention;
            OperatorLocationLongitude = model.OperatorLocationLongitude;
            OperatorLocationLatitude = model.OperatorLocationLatitude;
        }

        #endregion

        private short _zoneAttention = 5000;
        private short _zoneAlarm = 3000;
        private double _centerZoneLatitude;
        private double _centerZoneLongitude;
        private double _operatoLocationLatitude;
        private double _operatorLocationLongitude;

        [Range(100, 10000)]
        public short ZoneAttention
        {
            get => _zoneAttention;
            set
            {
                if (_zoneAttention == value) return;
                _zoneAttention = value;
                OnPropertyChanged();

            }
        }

        [Range(100, 10000)]
        public short ZoneAlarm
        {
            get => _zoneAlarm;
            set
            {
                if (_zoneAlarm == value) return;
                _zoneAlarm = value;
                OnPropertyChanged();
            }
        }

        [Range(-90, 90)]
        public double CenterZoneLatitude
        {
            get => _centerZoneLatitude;
            set
            {
                if (_centerZoneLatitude == value) return;
                _centerZoneLatitude = value;
                OnPropertyChanged();
            }
        }
        [Range(-180, 180)]
        public double CenterZoneLongitude
        {
            get => _centerZoneLongitude;
            set
            {
                if (_centerZoneLongitude == value) return;
                _centerZoneLongitude = value;
                OnPropertyChanged();
            }
        }

        [Range(-90, 90)]
        public double OperatorLocationLatitude
        {
            get => _operatoLocationLatitude;
            set
            {
                if (value == _operatoLocationLatitude) return;
                _operatoLocationLatitude = value;
                OnPropertyChanged();
            }
        }

        [Range(-180, 180)]
        public double OperatorLocationLongitude
        {
            get => _operatorLocationLongitude;
            set
            {
                if (value == _operatorLocationLongitude) return;
                _operatorLocationLongitude = value;
                OnPropertyChanged();
            }
        }
    }
}
