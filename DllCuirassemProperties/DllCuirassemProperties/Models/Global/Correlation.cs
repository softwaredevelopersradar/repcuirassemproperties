﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DllCuirassemProperties.Models.Global
{
    public class Correlation : AbstractBaseProperties<Correlation>
    {
        private byte amountCorrelationQuery;
        private float pathThroughCorrelationThreshold = 0.15f;
        private float coefficientBorderDеlayEstimation = 0.8f;



        [Range(0, 255)]
        public byte AmountCorrelationQuery
        {
            get => amountCorrelationQuery;
            set
            {
                if (amountCorrelationQuery == value) return;
                amountCorrelationQuery = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 1)]
        public float PathThroughCorrelationThreshold
        {
            get => pathThroughCorrelationThreshold;
            set
            {
                if (pathThroughCorrelationThreshold == value) return;
                pathThroughCorrelationThreshold = value;
                OnPropertyChanged();
            }
        }


        [Range(0.2,20)]
        public float CoefficientBorderDеlayEstimation
        {
            get => coefficientBorderDеlayEstimation;
            set
            {
                if (coefficientBorderDеlayEstimation == value) return;
                coefficientBorderDеlayEstimation = value;
                OnPropertyChanged();
            }
        }

        public override Correlation Clone()
        {
            return new Correlation
            {
                AmountCorrelationQuery = AmountCorrelationQuery,
                PathThroughCorrelationThreshold = PathThroughCorrelationThreshold,
                CoefficientBorderDеlayEstimation = CoefficientBorderDеlayEstimation
            };
        }

        public override bool EqualTo(Correlation model)
        {
            return model.AmountCorrelationQuery == AmountCorrelationQuery
               && model.PathThroughCorrelationThreshold == PathThroughCorrelationThreshold
               && model.CoefficientBorderDеlayEstimation == CoefficientBorderDеlayEstimation;
        }

        public override void Update(Correlation model)
        {
            AmountCorrelationQuery = model.AmountCorrelationQuery;
            PathThroughCorrelationThreshold = model.PathThroughCorrelationThreshold;
            CoefficientBorderDеlayEstimation = model.CoefficientBorderDеlayEstimation;
        }
    }
}
