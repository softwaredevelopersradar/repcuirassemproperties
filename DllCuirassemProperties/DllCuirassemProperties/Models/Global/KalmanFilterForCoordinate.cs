﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DllCuirassemProperties.Models.Global
{
    public class KalmanFilterForCoordinate : AbstractBaseProperties<KalmanFilterForCoordinate>
    {
        private float _maxUAVSpeed = 20;
        private float _timeHoldToDetectSource = 10;
        private float _maxResetTime = 20;
        private float _rmsX = 0.1f;
        private float _rmsY = 1f;
        private float _rmsZ = 0.1f;
        private short _minPointAirObject = 3;
        private bool _staticArrayError = true;
        private float _coordinateDefinitionError = 80;
        private float _strobeDistance = 80;
        private short _tauError = 3;
        private short _xYZError = 0;

        [Range(1, 200)]
        public float MaxUAVSpeed
        {
            get => _maxUAVSpeed;
            set
            {
                if (_maxUAVSpeed == value) return;
                _maxUAVSpeed = value;
                OnPropertyChanged();
            }
        }

        [Range(0.02, 100)]
        public float TimeHoldToDetectSource
        {
            get => _timeHoldToDetectSource;
            set
            {
                if (_timeHoldToDetectSource == value) return;
                _timeHoldToDetectSource = value;
                OnPropertyChanged();
            }
        }


        [Range(0.02, 100)]
        public float MaxResetTime
        {
            get => _maxResetTime;
            set
            {
                if (_maxResetTime == value) return;
                _maxResetTime = value;
                OnPropertyChanged();
            }
        }


        [Range(2, 100)]
        public short MinPointAirObject
        {
            get => _minPointAirObject;
            set
            {
                if (_minPointAirObject == value) return;
                _minPointAirObject = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 50)]
        public float RmsX
        {
            get => _rmsX;
            set
            {
                if (_rmsX == value) return;
                _rmsX = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 50)]
        public float RmsY
        {
            get => _rmsY;
            set
            {
                if (_rmsY == value) return;
                _rmsY = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 50)]
        public float RmsZ
        {
            get => _rmsZ;
            set
            {
                if (_rmsZ == value) return;
                _rmsZ = value;
                OnPropertyChanged();
            }
        }

        public bool StaticArrayError
        {
            get => _staticArrayError;
            set
            {
                if (_staticArrayError == value) return;
                _staticArrayError = value;
                OnPropertyChanged();
            }
        }

        [Range(0.1, 200)]
        public float CoordinateDefinitionError
        {
            get => _coordinateDefinitionError;
            set
            {
                if (_coordinateDefinitionError == value) return;
                _coordinateDefinitionError = value;
                OnPropertyChanged();
            }
        }

        [Range(1, 1000)]
        public float StrobeDistance
        {
            get => _strobeDistance;
            set
            {
                if (_strobeDistance == value) return;
                _strobeDistance = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 25)]
        public short TauError
        {
            get => _tauError;
            set
            {
                if (_tauError == value) return;
                _tauError = value;
                OnPropertyChanged();
            }
        }


        [Range(0, 30)]
        public short XYZError
        {
            get => _xYZError;
            set
            {
                if (_xYZError == value) return;
                _xYZError = value;
                OnPropertyChanged();
            }
        }


        public override KalmanFilterForCoordinate Clone()
        {
            return new KalmanFilterForCoordinate
            {
                MaxUAVSpeed = MaxUAVSpeed,
                TimeHoldToDetectSource = TimeHoldToDetectSource,
                MaxResetTime = MaxResetTime,
                MinPointAirObject = MinPointAirObject,
                RmsX = RmsX,
                RmsY = RmsY,
                RmsZ = RmsZ,
                StaticArrayError = StaticArrayError,
                CoordinateDefinitionError = CoordinateDefinitionError,
                StrobeDistance = StrobeDistance,
                TauError = TauError,
                XYZError = XYZError
            };
        }

        public override bool EqualTo(KalmanFilterForCoordinate model)
        {
            return model.MaxUAVSpeed == MaxUAVSpeed
                && model.TimeHoldToDetectSource == TimeHoldToDetectSource
                && model.MaxResetTime == MaxResetTime
                && model.MinPointAirObject == MinPointAirObject
                && model.RmsX == RmsX
                && model.RmsY == RmsY
                && model.RmsZ == RmsZ
                && model.StaticArrayError == StaticArrayError
                && model.CoordinateDefinitionError == CoordinateDefinitionError
                && model.StrobeDistance == StrobeDistance
                && model.TauError == TauError
                && model.XYZError == XYZError;
        }

        public override void Update(KalmanFilterForCoordinate model)
        {
            MaxUAVSpeed = model.MaxUAVSpeed;
            TimeHoldToDetectSource = model.TimeHoldToDetectSource;
            MaxResetTime = model.MaxResetTime;
            MinPointAirObject = model.MinPointAirObject;
            RmsX = model.RmsX;
            RmsY = model.RmsY;
            RmsZ = model.RmsZ;
            StaticArrayError = model.StaticArrayError;
            CoordinateDefinitionError = model.CoordinateDefinitionError;
            StrobeDistance = model.StrobeDistance;
            TauError = model.TauError;
            XYZError = model.XYZError;
        }
    }
}
