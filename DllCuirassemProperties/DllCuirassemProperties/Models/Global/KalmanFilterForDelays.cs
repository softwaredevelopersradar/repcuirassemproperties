﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DllCuirassemProperties.Models.Global
{
    public class KalmanFilterForDelays : AbstractBaseProperties<KalmanFilterForDelays>
    {
        private bool _staticArrayError = true;
        private float _timeHoldToDetectSource = 3;
        private float delayMeasurementError = 8;
        private float _maxResetTime = 3;
        private short _minPointObject = 5;
        private float _noiseAccelerationError = 4;

        public bool StaticArrayError
        {
            get => _staticArrayError;
            set
            {
                if (_staticArrayError == value) return;
                _staticArrayError = value;
                OnPropertyChanged();
            }
        }

        [Range(0.1, 20)]
        public float DelayMeasurementError
        {
            get => delayMeasurementError;
            set
            {
                if (delayMeasurementError == value) return;
                delayMeasurementError = value;
                OnPropertyChanged();
            }
        }


        [Range(0.1, 20)]
        public float NoiseAccelerationError
        {
            get => _noiseAccelerationError;
            set
            {
                if (_noiseAccelerationError == value) return;
                _noiseAccelerationError = value;
                OnPropertyChanged();
            }
        }


        [Range(0.01, 100)]
        public float TimeHoldToDetectSource
        {
            get => _timeHoldToDetectSource;
            set
            {
                if (_timeHoldToDetectSource == value) return;
                _timeHoldToDetectSource = value;
                OnPropertyChanged();
            }
        }


        [Range(0.01, 100)]
        public float MaxResetTime
        {
            get => _maxResetTime;
            set
            {
                if (_maxResetTime == value) return;
                _maxResetTime = value;
                OnPropertyChanged();
            }
        }

        [Range(2, 100)]
        public short MinPointObject
        {
            get => _minPointObject;
            set
            {
                if (_minPointObject == value) return;
                _minPointObject = value;
                OnPropertyChanged();
            }
        }


        public override KalmanFilterForDelays Clone()
        {
            return new KalmanFilterForDelays
            {
                StaticArrayError = StaticArrayError,
                DelayMeasurementError = DelayMeasurementError,
                NoiseAccelerationError = NoiseAccelerationError,
                TimeHoldToDetectSource = TimeHoldToDetectSource,
                MaxResetTime = MaxResetTime,
                MinPointObject = MinPointObject
            };
        }

        public override bool EqualTo(KalmanFilterForDelays model)
        {
            return StaticArrayError == model.StaticArrayError
                && DelayMeasurementError == model.DelayMeasurementError
                && NoiseAccelerationError == model.NoiseAccelerationError
                && TimeHoldToDetectSource == model.TimeHoldToDetectSource
                && MaxResetTime == model.MaxResetTime
                && MinPointObject == model.MinPointObject;
        }

        public override void Update(KalmanFilterForDelays model)
        {
            StaticArrayError = model.StaticArrayError;
            DelayMeasurementError = model.DelayMeasurementError;
            NoiseAccelerationError = model.NoiseAccelerationError;
            TimeHoldToDetectSource = model.TimeHoldToDetectSource;
            MaxResetTime = model.MaxResetTime;
            MinPointObject = model.MinPointObject;
        }
    }
}
