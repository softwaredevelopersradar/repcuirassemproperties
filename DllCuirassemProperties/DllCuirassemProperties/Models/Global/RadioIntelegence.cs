﻿using System.ComponentModel.DataAnnotations;

namespace DllCuirassemProperties.Models.Global
{
    public class RadioIntelegence : AbstractBaseProperties<RadioIntelegence>
    {
        #region IModelMethods

        public override RadioIntelegence Clone()
        {
            return new RadioIntelegence
            {
                FrequencyAccuracy = FrequencyAccuracy,
                BandAccuracy = BandAccuracy,
                SamplingFrequency = SamplingFrequency
            };
        }

        public override bool EqualTo(RadioIntelegence model)
        {
            return  model.BandAccuracy == BandAccuracy
                && model.FrequencyAccuracy == FrequencyAccuracy
                && model.SamplingFrequency == SamplingFrequency;
               
        }

        public override void Update(RadioIntelegence model)
        {
            BandAccuracy = model.BandAccuracy;
            FrequencyAccuracy = model.FrequencyAccuracy;
            SamplingFrequency = model.SamplingFrequency;
        }

        #endregion
       

        private float frequencyAccuracy = 0.1f;
        private float bandAccuracy = 0.1f;
        private SamplingFrequency samplingFrequency = SamplingFrequency.f125e6;

        [Range(0.1, 100)]
        public float BandAccuracy
        {
            get => bandAccuracy;
            set
            {
                if (bandAccuracy == value) return;
                bandAccuracy = value;
                OnPropertyChanged();
            }
        }

        [Range(0.1, 100)]
        public float FrequencyAccuracy
        {
            get => frequencyAccuracy;
            set
            {
                if (frequencyAccuracy == value) return;
                frequencyAccuracy = value;
                OnPropertyChanged();
            }
        }

        public SamplingFrequency SamplingFrequency
        {
            get => samplingFrequency;
            set
            {
                if (samplingFrequency == value) return;
                samplingFrequency = value;
                OnPropertyChanged();
            }
        }

    }
}