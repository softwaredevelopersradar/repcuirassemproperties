﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using DllCuirassemProperties.Models.Global;
using YamlDotNet.Serialization;

namespace DllCuirassemProperties.Models
{

    [CategoryOrder(nameof(Common), 1)]
    [CategoryOrder(nameof(RadioIntelegence), 2)]
    [CategoryOrder(nameof(KalmanFilterForDelay), 3)]
    [CategoryOrder(nameof(KalmanFilterForCoordinate), 4)]
    [CategoryOrder(nameof(Correlation), 5)]
    [CategoryOrder(nameof(DRMFilters), 6)]

    public class GlobalProperties : DependencyObject, IModelMethods<GlobalProperties>
    {
        public event PropertyChangedEventHandler EventPropertyChanged = (obg, str) => { };
        private void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            EventPropertyChanged(this, e);
        }

        public GlobalProperties()
        {
            Common = new Common();
            RadioIntelegence = new RadioIntelegence();
            KalmanFilterForCoordinate = new KalmanFilterForCoordinate();
            KalmanFilterForDelay = new KalmanFilterForDelays();
            Correlation = new Correlation();
            DRMFilters = new DRMFilters();

            RadioIntelegence.PropertyChanged += PropertyChanged;
            Common.PropertyChanged += PropertyChanged;
            KalmanFilterForCoordinate.PropertyChanged += PropertyChanged;
            KalmanFilterForDelay.PropertyChanged += PropertyChanged;
            Correlation.PropertyChanged += PropertyChanged;
            DRMFilters.PropertyChanged += PropertyChanged;
        }

        [Category(nameof(Common))]
        [DisplayName(" ")]
        public Common Common { get; set; }

        [Category(nameof(RadioIntelegence))]
        [DisplayName(" ")]
        public RadioIntelegence RadioIntelegence { get; set; }

        [Category(nameof(KalmanFilterForDelay))]
        [DisplayName(" ")]
        public KalmanFilterForDelays KalmanFilterForDelay { get; set; }

        [Category(nameof(KalmanFilterForCoordinate))]
        [DisplayName(" ")]
        public KalmanFilterForCoordinate KalmanFilterForCoordinate { get; set; }

        [Category(nameof(Correlation))]
        [DisplayName(" ")]
        public Correlation Correlation { get; set; }

        [Category(nameof(DRMFilters))]
        [DisplayName(" ")]
        public DRMFilters DRMFilters { get; set; }


        public GlobalProperties Clone()
        {
            return new GlobalProperties
            {
                Common = Common.Clone(),
                RadioIntelegence = RadioIntelegence.Clone(),
                KalmanFilterForCoordinate = KalmanFilterForCoordinate.Clone(),
                KalmanFilterForDelay = KalmanFilterForDelay.Clone(),
                Correlation = Correlation.Clone(),
                DRMFilters = DRMFilters.Clone()
            };
        }

        public bool EqualTo(GlobalProperties model)
        {
            return Common.EqualTo(model.Common)
                && RadioIntelegence.EqualTo(model.RadioIntelegence)
                && KalmanFilterForCoordinate.EqualTo(model.KalmanFilterForCoordinate)
                && KalmanFilterForDelay.EqualTo(model.KalmanFilterForDelay)
                && Correlation.EqualTo(model.Correlation)
                && DRMFilters.EqualTo(model.DRMFilters);
        }

        public void Update(GlobalProperties model)
        {
            Common.Update(model.Common);
            RadioIntelegence.Update(model.RadioIntelegence);
            KalmanFilterForCoordinate.Update(model.KalmanFilterForCoordinate);
            KalmanFilterForDelay.Update(model.KalmanFilterForDelay);
            Correlation.Update(model.Correlation);
            DRMFilters.Update(model.DRMFilters);
        }

        public static readonly DependencyProperty AcssesProperty = DependencyProperty.Register(nameof(Access), typeof(AccessTypes), typeof(GlobalProperties), new PropertyMetadata(AccessTypes.Admin, new PropertyChangedCallback(AccessChanged)));

        static void AccessChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            GlobalProperties control = (GlobalProperties)d;
            control.Access = (AccessTypes)e.NewValue;
        }

        [YamlIgnore]
        [Browsable(false)]
        public AccessTypes Access
        {
            get { return (AccessTypes)GetValue(AcssesProperty); }
            set { SetValue(AcssesProperty, value); }
        }
    }
}
