﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DllCuirassemProperties.Models.Local
{
    public class Spectrum : AbstractBaseProperties<Spectrum>
    {
        #region IModelMethods
        public override Spectrum Clone()
        {
            return new Spectrum
            {
                Chanel_1 = Chanel_1,
                Chanel_2 = Chanel_2,
                Chanel_3 = Chanel_3,
                Chanel_4 = Chanel_4,
                Chanel_Max = Chanel_Max,
                Chanel_Median =Chanel_Median,
                Chanel_Sum = Chanel_Sum
            };
        }

        public override bool EqualTo(Spectrum model)
        {
            return Chanel_1 == model.Chanel_1
                  && Chanel_2 == model.Chanel_2
                  && Chanel_3 == model.Chanel_3
                  && Chanel_4 == model.Chanel_4
                  && Chanel_Max == model.Chanel_Max
                  && Chanel_Median == model.Chanel_Median
                  && Chanel_Sum == model.Chanel_Sum;
        }

        public override void Update(Spectrum model)
        {
            Chanel_1 = model.Chanel_1;
            Chanel_2 = model.Chanel_2;
            Chanel_3 = model.Chanel_3;
            Chanel_4 = model.Chanel_4;
            Chanel_Max = model.Chanel_Max;
            Chanel_Median = model.Chanel_Median;
            Chanel_Sum = model.Chanel_Sum;
        }
        #endregion


        private bool chanel_1 = true;
        private bool chanel_2 = true;
        private bool chanel_3 = true;
        private bool chanel_4 = true;
        private bool chanel_Max = false;
        private bool chanel_Median = false;
        private bool chanel_Sum = false;


        [Browsable(false)]
        public bool Chanel_1
        {
            get => chanel_1;
            set
            {
                if (chanel_1 == value) return;
                chanel_1 = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool Chanel_2
        {
            get => chanel_2;
            set
            {
                if (chanel_2 == value) return;
                chanel_2 = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool Chanel_3
        {
            get => chanel_3;
            set
            {
                if (chanel_3 == value) return;
                chanel_3 = value;
                OnPropertyChanged();
            }
        }
        [Browsable(false)]
        public bool Chanel_4
        {
            get => chanel_4;
            set
            {
                if (chanel_4 == value) return;
                chanel_4 = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool Chanel_Max
        {
            get => chanel_Max;
            set
            {
                if (chanel_Max == value) return;
                chanel_Max = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool Chanel_Median
        {
            get => chanel_Median;
            set
            {
                if (chanel_Median == value) return;
                chanel_Median = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool Chanel_Sum
        {
            get => chanel_Sum;
            set
            {
                if (chanel_Sum == value) return;
                chanel_Sum = value;
                OnPropertyChanged();
            }
        }
    }
}
