﻿using System.ComponentModel.DataAnnotations;

namespace DllCuirassemProperties.Models.Local
{
    public class Map : AbstractBaseProperties<Map>
    {
        #region IModelMethods

        public override bool EqualTo(Map model)
        {
            return FileMap == model.FileMap
                && FolderMapTiles == model.FolderMapTiles
                && ImageLocalPoint == model.ImageLocalPoint
                && FolderImageSource == model.FolderImageSource
                && ImageRemotePoint == model.ImageRemotePoint
                && Projection == model.Projection
                && LengthOfTrack == model.LengthOfTrack
                && AmountOfDefinePoint == model.AmountOfDefinePoint;
        }

        public override Map Clone()
        {
            return new Map
            {
                FileMap = FileMap,
                FolderMapTiles = FolderMapTiles,
                ImageLocalPoint = ImageLocalPoint,
                FolderImageSource = FolderImageSource,
                ImageRemotePoint = ImageRemotePoint,
                Projection = Projection,
                LengthOfTrack = LengthOfTrack,
                AmountOfDefinePoint = AmountOfDefinePoint
            };
        }

        public override void Update(Map model)
        {
            FileMap = model.FileMap;
            FolderMapTiles = model.FolderMapTiles;
            ImageLocalPoint = model.ImageLocalPoint;
            FolderImageSource = model.FolderImageSource;
            ImageRemotePoint = model.ImageRemotePoint;
            Projection = model.Projection;
            LengthOfTrack = model.LengthOfTrack;
            AmountOfDefinePoint = model.AmountOfDefinePoint;
        }

        #endregion

        private string fileMap = "";
        private string folderMapTiles = "";
        private string folderImageSource = "";
        private string imageLocalPoint = "";
        private string imageRemotePoint = "";
        private Projections projection = Projections.Geo;
        private short lengthOfTrack = 1;
        private short amountOfDefinePoint = 1;

        public string FileMap
        {
            get => fileMap;
            set
            {
                if (fileMap == value) return;
                fileMap = value;
                OnPropertyChanged();

            }
        }

        public string FolderMapTiles
        {
            get => folderMapTiles;
            set
            {
                if (folderMapTiles == value) return;
                folderMapTiles = value;
                OnPropertyChanged();
            }
        }

        public string FolderImageSource
        {
            get => folderImageSource;
            set
            {
                if (folderImageSource == value) return;
                folderImageSource = value;
                OnPropertyChanged();
            }
        }

        public string ImageLocalPoint
        {
            get => imageLocalPoint;
            set
            {
                if (imageLocalPoint == value) return;
                imageLocalPoint = value;
                OnPropertyChanged();

            }
        }

        [Required]
        public string ImageRemotePoint
        {
            get => imageRemotePoint;
            set
            {
                if (imageRemotePoint == value) return;
                imageRemotePoint = value;
                OnPropertyChanged();

            }
        }

        public Projections Projection
        {
            get => projection;
            set
            {
                if (value == projection) return;
                projection = value;
                OnPropertyChanged();
            }
        }

        [Required]
        public short LengthOfTrack
        {
            get => lengthOfTrack;
            set
            {
                if (lengthOfTrack == value) return;
                lengthOfTrack = value;
                OnPropertyChanged();
            }
        }


        [Required]
        public short AmountOfDefinePoint
        {
            get => amountOfDefinePoint;
            set
            {
                if (amountOfDefinePoint == value) return;
                amountOfDefinePoint = value;
                OnPropertyChanged();
            }
        }

    }
}
