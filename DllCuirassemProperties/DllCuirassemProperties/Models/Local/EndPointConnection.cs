﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DllCuirassemProperties.Models.Local
{
    public class EndPointConnection : AbstractBaseProperties<EndPointConnection>
    {
        private string ipAddress = "";
        private int port;

        [NotifyParentProperty(true)]
        [Required]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddress
        {
            get => ipAddress;
            set
            {
                if (ipAddress == value) return;
                ipAddress = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Range(1, 1000000)]
        public int Port
        {
            get => port;
            set
            {
                if (port == value) return;
                port = value;
                OnPropertyChanged();
            }
        }


        public override bool EqualTo(EndPointConnection model)
        {
            return IpAddress == model.IpAddress
                && Port == model.Port;
        }

        public override EndPointConnection Clone()
        {
            return new EndPointConnection
            {
                IpAddress = IpAddress,
                Port = Port
            };
        }

        public override void Update(EndPointConnection model)
        {
            Port = model.Port;
            IpAddress = model.IpAddress;
        }
    }
}
