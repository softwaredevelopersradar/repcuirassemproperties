﻿using System.Collections.ObjectModel;

namespace DllCuirassemProperties.Models
{
    public class AWSList:ObservableCollection<byte>
    {
        public AWSList()
        {
            Add(1);
            Add(2);
            Add(3);
        }
    }
}
