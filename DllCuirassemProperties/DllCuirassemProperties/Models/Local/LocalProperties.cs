﻿using DllCuirassemProperties.Models.Local;
using System.ComponentModel;

namespace DllCuirassemProperties.Models
{
    public class LocalProperties: IModelMethods<LocalProperties>
    {
        public event PropertyChangedEventHandler OnPropertyChanged = (obg, str) => { };
        private void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(this, e);
        }

        public LocalProperties()
        {
            Common = new Common();
            DB = new EndPointConnection();
            DF = new EndPointConnection();
            Map = new Map();
            Diagram = new Diagram();
            Spectrum = new Spectrum();

            Common.PropertyChanged += PropertyChanged;
            DB.PropertyChanged += PropertyChanged;
            DF.PropertyChanged += PropertyChanged;
            Map.PropertyChanged += PropertyChanged;
            Diagram.PropertyChanged += PropertyChanged;
            Spectrum.PropertyChanged += PropertyChanged;
        }

        [Category(nameof(Common))]
        [DisplayName(" ")]
        public Common Common { get; set; }

        [Category(nameof(DB))]
        [DisplayName(" ")]
        public EndPointConnection DB { get; set; }

        [Category(nameof(DF))]
        [DisplayName(" ")]
        public EndPointConnection DF { get; set; }

        [Category(nameof(Map))]
        [DisplayName(" ")]
        public Map Map { get; set; }

        [Category(nameof(Diagram))]
        [DisplayName(" ")]
        public Diagram Diagram { get; set; }

        [Browsable(false)]
        [Category(nameof(Spectrum))]
        [DisplayName(" ")]
        public Spectrum Spectrum { get; set; }

        public LocalProperties Clone()
        {
            return new LocalProperties
            {
                Common = Common.Clone(),
                DB = DB.Clone(),
                DF = DF.Clone(),
                Map = Map.Clone(),
                Diagram = Diagram.Clone(),
                Spectrum = Spectrum.Clone()
            };
        }

        public void Update(LocalProperties localProperties)
        {
            Common.Update(localProperties.Common);
            DB.Update(localProperties.DB);
            DF.Update(localProperties.DF);
            Map.Update(localProperties.Map);
            Diagram.Update(localProperties.Diagram);
            Spectrum.Update(localProperties.Spectrum);
        }

        public bool EqualTo(LocalProperties localProperties)
        {
            return Common.EqualTo(localProperties.Common)
                && DB.EqualTo(localProperties.DB)
                && DF.EqualTo(localProperties.DF)
                && Map.EqualTo(localProperties.Map)
                && Diagram.EqualTo(localProperties.Diagram)
                && Spectrum.EqualTo(localProperties.Spectrum);
        }
    }
}
