﻿namespace DllCuirassemProperties.Models.Local
{
    public class Common : AbstractBaseProperties<Common>
    { 
        private AccessTypes access = AccessTypes.Operator;
        private Languages language;
        private FileTypes fileType;
        private byte aws = 1;

        public AccessTypes Access
        {
            get => access;
            set
            {
                if (value == access) return;
                access = value;
                OnPropertyChanged();
            }
        }

        public Languages Language
        {
            get => language;
            set
            {
                if (language == value) return;
                language = value;
                OnPropertyChanged();
            }
        }

        public FileTypes FileType
        {
            get => fileType;
            set
            {
                if (value == fileType) return;
                fileType = value;
                OnPropertyChanged();
            }
        }

        public byte AWS
        {
            get => aws;
            set
            {
                if (value == aws) return;
                aws = value;
                OnPropertyChanged();
            }
        }


        public override bool EqualTo(Common model)
        {
            return Access == model.Access
                && Language == model.Language
                && FileType == model.FileType
                && AWS == model.AWS;
        }

        public override Common Clone()
        {
            return new Common
            {
                Access = Access,
                FileType = FileType,
                Language = Language,
                AWS = AWS
            };
        }

        public override void Update(Common model)
        {
            Access = model.Access;
            FileType = model.FileType;
            Language = model.Language;
            AWS = model.AWS;
        }
    }
}
