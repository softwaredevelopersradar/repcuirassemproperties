﻿namespace DllCuirassemProperties.Models.Local
{
    public class Diagram: AbstractBaseProperties<Diagram>
    {
        private int _panoramas;
        public int Panoramas
        {
            get => _panoramas;
            set
            {
                if (_panoramas == value) return;
                _panoramas = value;
                OnPropertyChanged();
            }
        }

        public override Diagram Clone()
        {
            return new Diagram
            {
                Panoramas = Panoramas
            };
        }

        public override bool EqualTo(Diagram model)
        {
            return Panoramas == model.Panoramas;
        }

        public override void Update(Diagram model)
        {
            Panoramas = model.Panoramas;
        }
    }
}
