﻿using System.Windows.Controls;

namespace DllCuirassemProperties
{
    public class EditorGeotifFileCommands
    {
        private static RelayCommand executeCommand;

        public static RelayCommand ExecuteCommand
        {
            get
            {
                return executeCommand ??
                    (executeCommand = new RelayCommand(ShowDialog, CanExecute));
            }
            set
            {
                if (executeCommand == value) return;
                executeCommand = value;
            }
        }


        public static void ShowDialog(object e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog()
            {
                Filter = "Geotif files(*.tif)|*.tif"
            };

            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                (e as TextBox).Text = ofd.FileName;
                return;
            }

        }

        public static bool CanExecute(object e)
        {
            return true;
        }
    }
}
